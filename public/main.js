function calculation(){
    let inp1 = document.getElementById("inp1").value;
    let inp2 = document.getElementById("inp2").value;
    if((inp1.match(/^[0-9]*[.]?[0-9]+$/) === null) || (inp2.match(/^[0-9]*[.]?[0-9]+$/) === null)){
        document.getElementById("res").innerHTML="Ошибка";
        alert("Некорректные входные данные (Примечание: при использовании вещественных чисел использовать точку)");
    }
    else{
        let all = inp1 * inp2;
        document.getElementById("res").innerHTML="К оплате: " + all.toFixed(2);
    }
}

window.addEventListener("DOMContentLoaded", function (event) {
    console.log("DOM fully loaded and parsed");
    let b = document.getElementById("button");
    b.addEventListener("click", calculation);
});
